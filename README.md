# Attendance Tools

This API will have user and attandance API.
The app will send email reminder to clock-in and clock-out (by default) on 9AM and 6PM

## Prerequisite
this app is developed on go 1.20.2

## How To Run:

set config value on `.env`

generate Application Password from gmail (https://support.google.com/mail/answer/185833?hl=en)

put the email and password on `.env` 
```
FROM_EMAIL = email@gmail.com
FROM_EMAIL_PASSWORD = password
```

run app using

```
go run main.go
```

## How To Deploy:
set config value on `fly.toml`

build the app using

```
go build -o d-attendance
```

then run 

```
fly launch
```

currently this App is deployed and can be access on https://d-attendance.fly.dev


## API Documentation

https://documenter.getpostman.com/view/795990/2s93Xu3knh

