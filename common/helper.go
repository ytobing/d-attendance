package common

import (
	"fmt"
	"log"
	"net/smtp"
	"os"
	"time"

	"github.com/golang-jwt/jwt"
	"golang.org/x/crypto/bcrypt"
)

func HashPassword(pass string) (string, error) {
	password := []byte(pass)

	hashedPassword, err := bcrypt.GenerateFromPassword(password, bcrypt.DefaultCost)
	if err != nil {
		log.Println(err)
		return "", err
	}
	return string(hashedPassword), nil

}

func ValidatePassword(password, hashedPassword string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
	if err != nil {
		return false
	}
	return true

}

func NewPayload(id int, email string, name string, role string, duration time.Duration) *jwt.MapClaims {
	payload := &jwt.MapClaims{
		"iss": email,
		"exp": time.Now().Add(duration).Unix(),
		"data": map[string]interface{}{
			"id":    id,
			"email": email,
			"name":  name,
			"role":  role,
		},
	}

	return payload

}

func CreateToken(id int, email string, name string, role string, duration time.Duration) (string, error) {
	payload := NewPayload(id, email, name, role, duration)

	jwtToken := jwt.NewWithClaims(jwt.SigningMethodHS256, payload)
	return jwtToken.SignedString([]byte(os.Getenv("JWT_SECRET_KEY")))
}

func ValidateToken(tokenString string) (res map[string]interface{}, err error) {
	claims := jwt.MapClaims{}

	_, err = jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(os.Getenv("JWT_SECRET_KEY")), nil
	})
	if err != nil {
		log.Println(err)
		return
	}

	res = claims["data"].(map[string]interface{})

	return

}

func SendEmail(MailAuth smtp.Auth, to []string, message []byte) {
	smtpHost := os.Getenv("SMTP_HOST")
	smtpPort := os.Getenv("SMTP_PORT")

	err := smtp.SendMail(smtpHost+":"+smtpPort, MailAuth, os.Getenv("FROM_EMAIL"), to, message)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("Email Sent Successfully!")
}
