package common

import "time"

type LoginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type UserRequest struct {
	ID       int    `json:"id"`
	Email    string `json:"email"`
	Name     string `json:"name"`
	Password string `json:"password"`
}

type AttendaceRequest struct {
	ID            int    `json:"id"`
	UserID        int    `json:"user_id"`
	AttendaceType string `json:"attendace_type"`
	Time          time.Time
	Date          string
}

type LoginData struct {
	Token string `json:"token"`
}
