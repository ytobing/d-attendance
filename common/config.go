package common

import (
	"context"
	"fmt"
	"log"
	"net/smtp"
	"os"

	_ "entgo.io/ent"

	"entgo.io/ent/dialect"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	_ "github.com/mattn/go-sqlite3"

	"gitlab.com/ytobing/d-attendance/ent"
)

type Config struct {
	DB       *ent.Client
	Router   *echo.Echo
	MailAuth smtp.Auth
}

func InitConfig(ctx context.Context) Config {
	c := Config{}
	c.database(ctx)
	c.router()
	c.InitEmail()

	return c
}

func (c *Config) database(ctx context.Context) (err error) {

	c.DB, err = ent.Open(dialect.SQLite, fmt.Sprintf("%s?mode=memory&cache=shared&_fk=1", os.Getenv("DB_NAME")))
	if err != nil {
		log.Fatal(err)
		return
	}

	// enable debugging
	// client = client.Debug()

	log.Println("Connected to db")

	if err = c.DB.Schema.Create(ctx); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
		return
	}

	hashedPassword, err := HashPassword("admin")
	if err != nil {
		log.Println(err)
		return
	}

	c.DB.User.Create().
		SetName("Admin").
		SetEmail("admin@email.com").
		SetPassword(hashedPassword).
		SetRole(USER_ROLE_ADMIN).
		SetStatus(true).
		Save(ctx)

	return
}

func (c *Config) router() {
	c.Router = echo.New()
	c.Router.Use(middleware.Logger())
	c.Router.Use(middleware.Recover())
	return

}

func (c *Config) InitEmail() {
	from := os.Getenv("FROM_EMAIL")
	password := os.Getenv("FROM_EMAIL_PASSWORD")
	smtpHost := os.Getenv("SMTP_HOST")

	c.MailAuth = smtp.PlainAuth("", from, password, smtpHost)

}
