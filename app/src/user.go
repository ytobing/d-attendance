package src

import (
	"errors"
	"fmt"
	"log"
	"time"

	"gitlab.com/ytobing/d-attendance/common"
	"gitlab.com/ytobing/d-attendance/ent"

	"gitlab.com/ytobing/d-attendance/ent/user"

	"golang.org/x/net/context"
)

func (s *Src) Login(ctx context.Context, email, password string) (res common.LoginData, err error) {
	data, err := s.DB.User.Query().Select().Where(user.Email(email)).Only(ctx)
	if err != nil {
		log.Println(err)
		return
	}

	valid := common.ValidatePassword(password, data.Password)

	if !valid {
		err = errors.New("Invalid email/passwor")
	}

	token, err := common.CreateToken(data.ID, data.Email, data.Name, data.Role, time.Hour*24)
	if err != nil {
		log.Println(err)
		return
	}

	aa, err := common.ValidateToken(token)
	if err != nil {
		log.Println(err)
		return
	}
	fmt.Println(aa)

	res = common.LoginData{
		Token: token,
	}

	return

}

func (s *Src) Register(ctx context.Context, req common.UserRequest) (err error) {
	hashedPassword, err := common.HashPassword(req.Password)
	if err != nil {
		log.Println(err)
		return
	}
	_, err = s.DB.User.Create().
		SetName(req.Name).
		SetEmail(req.Email).
		SetPassword(hashedPassword).
		SetRole(common.USER_ROLE_EMPLOYEE).
		SetStatus(true).
		SetCreatedAt(time.Now()).
		Save(ctx)
	if err != nil {
		log.Println(err)
		return
	}

	return

}

func (s *Src) Update(ctx context.Context, req common.UserRequest) (err error) {
	user := s.DB.User.Update().Where(user.ID(req.ID)).
		SetName(req.Name).
		SetEmail(req.Email).
		SetRole(common.USER_ROLE_EMPLOYEE).
		SetStatus(true).
		SetUpdateAt(time.Now())
	if req.Password != "" {
		hashedPassword, err := common.HashPassword(req.Password)
		if err != nil {
			log.Println(err)
			return err
		}
		user.SetPassword(hashedPassword)
	}

	_, err = user.Save(ctx)
	if err != nil {
		log.Println(err)
		return
	}

	return

}

func (s *Src) GetUser(ctx context.Context, id int) (res []*ent.User, err error) {
	u := s.DB.User.Query().Select(user.FieldID, user.FieldName, user.FieldEmail, user.FieldRole, user.FieldStatus).Where(user.DeleteAtIsNil())
	if id != 0 {
		u.Where(user.ID(id))
	}
	res, err = u.All(ctx)
	if err != nil {
		log.Println(err)
		return
	}

	return

}

func (s *Src) DeleteUser(ctx context.Context, id int) (err error) {
	_, err = s.DB.User.Update().Where(user.ID(id)).
		SetDeleteAt(time.Now()).Save(ctx)
	if err != nil {
		log.Println(err)
		return
	}

	return

}
