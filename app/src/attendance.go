package src

import (
	"fmt"
	"log"
	"time"

	"gitlab.com/ytobing/d-attendance/common"
	"gitlab.com/ytobing/d-attendance/ent"
	"gitlab.com/ytobing/d-attendance/ent/attendance"
	"gitlab.com/ytobing/d-attendance/ent/user"
	"golang.org/x/net/context"
)

func (s *Src) UpsertAttendance(ctx context.Context, req common.AttendaceRequest) (err error) {
	isExist, err := s.DB.Attendance.Query().Select().Where(attendance.HasUserWith(user.ID(req.UserID)), attendance.ClockedInDate(time.Now().Format("2006-01-02")), attendance.DeleteAtIsNil()).
		Exist(ctx)
	if err != nil {
		log.Println(err)
		return
	}

	if !isExist {
		_, err = s.DB.Attendance.Create().
			SetUserID(req.UserID).
			SetClockedIn(req.Time).
			SetClockedInDate(req.Date).
			SetCreatedAt(time.Now()).
			Save(ctx)
	} else {
		_, err = s.DB.Attendance.Update().Where(attendance.HasUserWith(user.ID(req.UserID)), attendance.ClockedInDate(time.Now().Format("2006-01-02")), attendance.DeleteAtIsNil()).
			SetClockedOut(req.Time).
			SetUpdateAt(time.Now()).Save(ctx)
	}

	return

}

func (s *Src) GetAttendance(ctx context.Context, req common.AttendaceRequest) (res []*ent.Attendance, err error) {

	res, err = s.DB.Attendance.Query().Where(attendance.HasUserWith(user.ID(req.UserID)), attendance.ClockedInDate(time.Now().Format("2006-01-02")), attendance.DeleteAtIsNil()).Select().All(ctx)

	return

}

func (s *Src) DeleteAttendance(ctx context.Context, id int) (err error) {
	_, err = s.DB.Attendance.Update().Where(attendance.ID(id)).
		SetDeleteAt(time.Now()).Save(ctx)
	if err != nil {
		log.Println(err)
		return
	}
	return

}

func (s *Src) EmailReminder(ctx context.Context, clockType string) {
	data := []string{}
	err := s.DB.User.Query().Where(user.IDNotIn(1)).Select(user.FieldEmail).Scan(ctx, &data)
	if err != nil {
		log.Println(err)
		return
	}

	message := []byte(fmt.Sprintf("Subject: %s\r\n\r\n"+
		"A reminder for you to %s today\r\n", "Clock In - Clock Out Reminder", clockType))
	common.SendEmail(s.MailAuth, data, message)
}
