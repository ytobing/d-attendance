package src

import (
	"net/smtp"

	"gitlab.com/ytobing/d-attendance/common"
	"gitlab.com/ytobing/d-attendance/ent"
)

type Src struct {
	DB       *ent.Client
	MailAuth smtp.Auth
}

func Init(c common.Config) Src {
	s := Src{
		DB:       c.DB,
		MailAuth: c.MailAuth,
	}

	return s
}
