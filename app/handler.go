package app

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"strconv"
	"time"

	"log"

	"github.com/go-co-op/gocron"
	"github.com/labstack/echo/v4"

	"gitlab.com/ytobing/d-attendance/app/src"
	"gitlab.com/ytobing/d-attendance/common"
)

type Handler struct {
	Config  common.Config
	Service src.Src
}

type response struct {
	Success bool        `json:"success"`
	Data    interface{} `json:"data"`
	Message string      `json:"message"`
}

const (
	LOGIN_URL    = "/user/login"
	REGISTER_URL = "/user/register"
	UPDATE_URL   = "/user/update"
	GET_URL      = "/user/get"
	DELETE_URL   = "/user/delete"

	UPSERT_ATTENDANCE = "/attendance/add"
	GET_ATTENDANCE    = "/attendance/get"
	DELETE_ATTENDANCE = "/attendance/delete"
)

func Init(c common.Config) Handler {
	s := src.Init(c)
	h := Handler{
		Config:  c,
		Service: s,
	}

	h.Config.Router.POST(LOGIN_URL, h.LoginHandler)
	h.Config.Router.POST(REGISTER_URL, h.RegisterHandler)
	h.Config.Router.POST(UPDATE_URL, h.UpdateHandler)
	h.Config.Router.GET(GET_URL, h.GetUserHandler)
	h.Config.Router.DELETE(DELETE_URL, h.DeleteUserHandler)

	h.Config.Router.POST(UPSERT_ATTENDANCE, h.UpsertAttendanceHandler)
	h.Config.Router.GET(GET_ATTENDANCE, h.GetAttendanceHandler)
	h.Config.Router.POST(UPSERT_ATTENDANCE, h.UpsertAttendanceHandler)
	h.Config.Router.DELETE(DELETE_ATTENDANCE, h.DeleteAttendanceHandler)

	tz, err := time.LoadLocation("Asia/Jakarta")
	if err != nil {
		log.Fatal(err)
	}
	cr := gocron.NewScheduler(tz)

	cr.Cron(fmt.Sprintf("0 %s * * *", os.Getenv("CLOCKED_IN_HOUR"))).Do(h.RunEmailSchedule, "clocked in")
	cr.Cron(fmt.Sprintf("0 %s * * *", os.Getenv("CLOCKED_OUT_HOUR"))).Do(h.RunEmailSchedule, "clocked out")
	cr.StartAsync()

	return h
}

func setResponse(data interface{}, err error) response {
	if err != nil {
		return response{
			Success: false,
			Data:    data,
			Message: err.Error(),
		}
	}
	return response{
		Success: true,
		Data:    data,
	}

}

func (h *Handler) LoginHandler(c echo.Context) error {
	ctx := context.Background()
	req := common.LoginRequest{}

	err := json.NewDecoder(c.Request().Body).Decode(&req)
	if err != nil {
		log.Println("empty json body")
		return nil
	}

	res, err := h.Service.Login(ctx, req.Email, req.Password)
	if err != nil {
		log.Println(err)
		return c.JSON(200, setResponse(res, err))
	}

	return c.JSON(200, setResponse(res, err))
}

func (h *Handler) RegisterHandler(c echo.Context) error {

	claims, err := common.ValidateToken(c.Request().Header.Get("Authorization"))
	if err != nil {
		err = errors.New("invalid token")
		return c.JSON(200, setResponse(false, err))
	}

	if claims["role"] != common.USER_ROLE_ADMIN {
		fmt.Println("asfas")
		err = errors.New("Unauthorized")
		return c.JSON(200, setResponse(false, err))
	}

	ctx := context.Background()
	req := common.UserRequest{}
	err = json.NewDecoder(c.Request().Body).Decode(&req)
	if err != nil {
		err = errors.New("empty json body")
		return c.JSON(200, setResponse(false, err))
	}

	err = h.Service.Register(ctx, req)
	if err != nil {
		log.Println(err)
		return c.JSON(200, setResponse(false, err))
	}

	return c.JSON(200, setResponse(true, err))
}

func (h *Handler) UpdateHandler(c echo.Context) error {

	claims, err := common.ValidateToken(c.Request().Header.Get("Authorization"))
	if err != nil {
		err = errors.New("invalid token")
		return c.JSON(200, setResponse(false, err))
	}

	if claims["role"] != common.USER_ROLE_ADMIN {
		fmt.Println("asfas")
		err = errors.New("Unauthorized")
		return c.JSON(200, setResponse(false, err))
	}

	ctx := context.Background()
	req := common.UserRequest{}
	err = json.NewDecoder(c.Request().Body).Decode(&req)
	if err != nil {
		err = errors.New("empty json body")
		return c.JSON(200, setResponse(false, err))
	}

	err = h.Service.Update(ctx, req)
	if err != nil {
		log.Println(err)
		return c.JSON(200, setResponse(false, err))
	}

	return c.JSON(200, setResponse(true, err))
}

func (h *Handler) GetUserHandler(c echo.Context) error {

	claims, err := common.ValidateToken(c.Request().Header.Get("Authorization"))
	if err != nil {
		err = errors.New("invalid token")
		return c.JSON(200, setResponse(false, err))
	}

	if claims["role"] != common.USER_ROLE_ADMIN {
		fmt.Println("asfas")
		err = errors.New("Unauthorized")
		return c.JSON(200, setResponse(false, err))
	}

	ctx := context.Background()

	id := 0
	if c.Request().URL.Query().Get("id") != "" {
		id, err = strconv.Atoi(c.Request().URL.Query().Get("id"))
		if err != nil {
			log.Println(err)
			return c.JSON(200, setResponse(nil, err))
		}
	}

	res, err := h.Service.GetUser(ctx, id)
	if err != nil {
		log.Println(err)
		return c.JSON(200, setResponse(nil, err))
	}

	return c.JSON(200, setResponse(res, err))
}

func (h *Handler) DeleteUserHandler(c echo.Context) error {

	claims, err := common.ValidateToken(c.Request().Header.Get("Authorization"))
	if err != nil {
		err = errors.New("invalid token")
		return c.JSON(200, setResponse(false, err))
	}

	if claims["role"] != common.USER_ROLE_ADMIN {
		fmt.Println("asfas")
		err = errors.New("Unauthorized")
		return c.JSON(200, setResponse(false, err))
	}

	ctx := context.Background()

	id := 0
	if c.Request().URL.Query().Get("id") != "" {
		id, err = strconv.Atoi(c.Request().URL.Query().Get("id"))
		if err != nil {
			log.Println(err)
			return c.JSON(200, setResponse(nil, err))
		}
	}

	err = h.Service.DeleteUser(ctx, id)
	if err != nil {
		log.Println(err)
		return c.JSON(200, setResponse(false, err))
	}

	return c.JSON(200, setResponse(true, err))
}

func (h *Handler) UpsertAttendanceHandler(c echo.Context) error {

	claims, err := common.ValidateToken(c.Request().Header.Get("Authorization"))
	if err != nil {
		err = errors.New("invalid token")
		return c.JSON(200, setResponse(false, err))
	}

	ctx := context.Background()

	id := int(claims["id"].(float64))

	req := common.AttendaceRequest{
		UserID: id,
		Time:   time.Now(),
		Date:   time.Now().Format("2006-01-02"),
	}

	err = h.Service.UpsertAttendance(ctx, req)
	if err != nil {
		log.Println(err)
		return c.JSON(200, setResponse(false, err))
	}

	return c.JSON(200, setResponse(true, err))
}

func (h *Handler) GetAttendanceHandler(c echo.Context) error {
	claims, err := common.ValidateToken(c.Request().Header.Get("Authorization"))
	if err != nil {
		err = errors.New("invalid token")
		return c.JSON(200, setResponse(false, err))
	}

	ctx := context.Background()

	id := int(claims["id"].(float64))

	req := common.AttendaceRequest{
		UserID: id,
		Date:   time.Now().Format("2006-01-02"),
	}

	res, err := h.Service.GetAttendance(ctx, req)
	if err != nil {
		log.Println(err)
		return c.JSON(200, setResponse(nil, err))
	}

	return c.JSON(200, setResponse(res, err))
}

func (h *Handler) DeleteAttendanceHandler(c echo.Context) error {

	claims, err := common.ValidateToken(c.Request().Header.Get("Authorization"))
	if err != nil {
		err = errors.New("invalid token")
		return c.JSON(200, setResponse(false, err))
	}

	if claims["role"] != common.USER_ROLE_ADMIN {
		fmt.Println("asfas")
		err = errors.New("Unauthorized")
		return c.JSON(200, setResponse(false, err))
	}

	ctx := context.Background()

	id := 0
	if c.Request().URL.Query().Get("id") != "" {
		id, err = strconv.Atoi(c.Request().URL.Query().Get("id"))
		if err != nil {
			log.Println(err)
			return c.JSON(200, setResponse(nil, err))
		}
	}

	err = h.Service.DeleteAttendance(ctx, id)
	if err != nil {
		log.Println(err)
		return c.JSON(200, setResponse(false, err))
	}

	return c.JSON(200, setResponse(true, err))
}

func (h *Handler) RunEmailSchedule(clockType string) {
	ctx := context.Background()
	h.Service.EmailReminder(ctx, clockType)
	return
}
