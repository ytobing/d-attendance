// Code generated by ent, DO NOT EDIT.

package ent

import (
	"context"
	"errors"
	"fmt"
	"time"

	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
	"gitlab.com/ytobing/d-attendance/ent/attendance"
	"gitlab.com/ytobing/d-attendance/ent/user"
)

// AttendanceCreate is the builder for creating a Attendance entity.
type AttendanceCreate struct {
	config
	mutation *AttendanceMutation
	hooks    []Hook
}

// SetClockedInDate sets the "clocked_in_date" field.
func (ac *AttendanceCreate) SetClockedInDate(s string) *AttendanceCreate {
	ac.mutation.SetClockedInDate(s)
	return ac
}

// SetNillableClockedInDate sets the "clocked_in_date" field if the given value is not nil.
func (ac *AttendanceCreate) SetNillableClockedInDate(s *string) *AttendanceCreate {
	if s != nil {
		ac.SetClockedInDate(*s)
	}
	return ac
}

// SetClockedIn sets the "clocked_in" field.
func (ac *AttendanceCreate) SetClockedIn(t time.Time) *AttendanceCreate {
	ac.mutation.SetClockedIn(t)
	return ac
}

// SetNillableClockedIn sets the "clocked_in" field if the given value is not nil.
func (ac *AttendanceCreate) SetNillableClockedIn(t *time.Time) *AttendanceCreate {
	if t != nil {
		ac.SetClockedIn(*t)
	}
	return ac
}

// SetClockedOut sets the "clocked_out" field.
func (ac *AttendanceCreate) SetClockedOut(t time.Time) *AttendanceCreate {
	ac.mutation.SetClockedOut(t)
	return ac
}

// SetNillableClockedOut sets the "clocked_out" field if the given value is not nil.
func (ac *AttendanceCreate) SetNillableClockedOut(t *time.Time) *AttendanceCreate {
	if t != nil {
		ac.SetClockedOut(*t)
	}
	return ac
}

// SetCreatedAt sets the "created_at" field.
func (ac *AttendanceCreate) SetCreatedAt(t time.Time) *AttendanceCreate {
	ac.mutation.SetCreatedAt(t)
	return ac
}

// SetNillableCreatedAt sets the "created_at" field if the given value is not nil.
func (ac *AttendanceCreate) SetNillableCreatedAt(t *time.Time) *AttendanceCreate {
	if t != nil {
		ac.SetCreatedAt(*t)
	}
	return ac
}

// SetUpdateAt sets the "update_at" field.
func (ac *AttendanceCreate) SetUpdateAt(t time.Time) *AttendanceCreate {
	ac.mutation.SetUpdateAt(t)
	return ac
}

// SetNillableUpdateAt sets the "update_at" field if the given value is not nil.
func (ac *AttendanceCreate) SetNillableUpdateAt(t *time.Time) *AttendanceCreate {
	if t != nil {
		ac.SetUpdateAt(*t)
	}
	return ac
}

// SetDeleteAt sets the "delete_at" field.
func (ac *AttendanceCreate) SetDeleteAt(t time.Time) *AttendanceCreate {
	ac.mutation.SetDeleteAt(t)
	return ac
}

// SetNillableDeleteAt sets the "delete_at" field if the given value is not nil.
func (ac *AttendanceCreate) SetNillableDeleteAt(t *time.Time) *AttendanceCreate {
	if t != nil {
		ac.SetDeleteAt(*t)
	}
	return ac
}

// SetID sets the "id" field.
func (ac *AttendanceCreate) SetID(i int) *AttendanceCreate {
	ac.mutation.SetID(i)
	return ac
}

// SetUserID sets the "user" edge to the User entity by ID.
func (ac *AttendanceCreate) SetUserID(id int) *AttendanceCreate {
	ac.mutation.SetUserID(id)
	return ac
}

// SetNillableUserID sets the "user" edge to the User entity by ID if the given value is not nil.
func (ac *AttendanceCreate) SetNillableUserID(id *int) *AttendanceCreate {
	if id != nil {
		ac = ac.SetUserID(*id)
	}
	return ac
}

// SetUser sets the "user" edge to the User entity.
func (ac *AttendanceCreate) SetUser(u *User) *AttendanceCreate {
	return ac.SetUserID(u.ID)
}

// Mutation returns the AttendanceMutation object of the builder.
func (ac *AttendanceCreate) Mutation() *AttendanceMutation {
	return ac.mutation
}

// Save creates the Attendance in the database.
func (ac *AttendanceCreate) Save(ctx context.Context) (*Attendance, error) {
	ac.defaults()
	return withHooks[*Attendance, AttendanceMutation](ctx, ac.sqlSave, ac.mutation, ac.hooks)
}

// SaveX calls Save and panics if Save returns an error.
func (ac *AttendanceCreate) SaveX(ctx context.Context) *Attendance {
	v, err := ac.Save(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Exec executes the query.
func (ac *AttendanceCreate) Exec(ctx context.Context) error {
	_, err := ac.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (ac *AttendanceCreate) ExecX(ctx context.Context) {
	if err := ac.Exec(ctx); err != nil {
		panic(err)
	}
}

// defaults sets the default values of the builder before save.
func (ac *AttendanceCreate) defaults() {
	if _, ok := ac.mutation.ClockedInDate(); !ok {
		v := attendance.DefaultClockedInDate
		ac.mutation.SetClockedInDate(v)
	}
	if _, ok := ac.mutation.CreatedAt(); !ok {
		v := attendance.DefaultCreatedAt()
		ac.mutation.SetCreatedAt(v)
	}
}

// check runs all checks and user-defined validators on the builder.
func (ac *AttendanceCreate) check() error {
	if _, ok := ac.mutation.ClockedInDate(); !ok {
		return &ValidationError{Name: "clocked_in_date", err: errors.New(`ent: missing required field "Attendance.clocked_in_date"`)}
	}
	if _, ok := ac.mutation.CreatedAt(); !ok {
		return &ValidationError{Name: "created_at", err: errors.New(`ent: missing required field "Attendance.created_at"`)}
	}
	return nil
}

func (ac *AttendanceCreate) sqlSave(ctx context.Context) (*Attendance, error) {
	if err := ac.check(); err != nil {
		return nil, err
	}
	_node, _spec := ac.createSpec()
	if err := sqlgraph.CreateNode(ctx, ac.driver, _spec); err != nil {
		if sqlgraph.IsConstraintError(err) {
			err = &ConstraintError{msg: err.Error(), wrap: err}
		}
		return nil, err
	}
	if _spec.ID.Value != _node.ID {
		id := _spec.ID.Value.(int64)
		_node.ID = int(id)
	}
	ac.mutation.id = &_node.ID
	ac.mutation.done = true
	return _node, nil
}

func (ac *AttendanceCreate) createSpec() (*Attendance, *sqlgraph.CreateSpec) {
	var (
		_node = &Attendance{config: ac.config}
		_spec = sqlgraph.NewCreateSpec(attendance.Table, sqlgraph.NewFieldSpec(attendance.FieldID, field.TypeInt))
	)
	if id, ok := ac.mutation.ID(); ok {
		_node.ID = id
		_spec.ID.Value = id
	}
	if value, ok := ac.mutation.ClockedInDate(); ok {
		_spec.SetField(attendance.FieldClockedInDate, field.TypeString, value)
		_node.ClockedInDate = value
	}
	if value, ok := ac.mutation.ClockedIn(); ok {
		_spec.SetField(attendance.FieldClockedIn, field.TypeTime, value)
		_node.ClockedIn = &value
	}
	if value, ok := ac.mutation.ClockedOut(); ok {
		_spec.SetField(attendance.FieldClockedOut, field.TypeTime, value)
		_node.ClockedOut = &value
	}
	if value, ok := ac.mutation.CreatedAt(); ok {
		_spec.SetField(attendance.FieldCreatedAt, field.TypeTime, value)
		_node.CreatedAt = value
	}
	if value, ok := ac.mutation.UpdateAt(); ok {
		_spec.SetField(attendance.FieldUpdateAt, field.TypeTime, value)
		_node.UpdateAt = &value
	}
	if value, ok := ac.mutation.DeleteAt(); ok {
		_spec.SetField(attendance.FieldDeleteAt, field.TypeTime, value)
		_node.DeleteAt = &value
	}
	if nodes := ac.mutation.UserIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2O,
			Inverse: true,
			Table:   attendance.UserTable,
			Columns: []string{attendance.UserColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: sqlgraph.NewFieldSpec(user.FieldID, field.TypeInt),
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_node.user_attendance = &nodes[0]
		_spec.Edges = append(_spec.Edges, edge)
	}
	return _node, _spec
}

// AttendanceCreateBulk is the builder for creating many Attendance entities in bulk.
type AttendanceCreateBulk struct {
	config
	builders []*AttendanceCreate
}

// Save creates the Attendance entities in the database.
func (acb *AttendanceCreateBulk) Save(ctx context.Context) ([]*Attendance, error) {
	specs := make([]*sqlgraph.CreateSpec, len(acb.builders))
	nodes := make([]*Attendance, len(acb.builders))
	mutators := make([]Mutator, len(acb.builders))
	for i := range acb.builders {
		func(i int, root context.Context) {
			builder := acb.builders[i]
			builder.defaults()
			var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
				mutation, ok := m.(*AttendanceMutation)
				if !ok {
					return nil, fmt.Errorf("unexpected mutation type %T", m)
				}
				if err := builder.check(); err != nil {
					return nil, err
				}
				builder.mutation = mutation
				var err error
				nodes[i], specs[i] = builder.createSpec()
				if i < len(mutators)-1 {
					_, err = mutators[i+1].Mutate(root, acb.builders[i+1].mutation)
				} else {
					spec := &sqlgraph.BatchCreateSpec{Nodes: specs}
					// Invoke the actual operation on the latest mutation in the chain.
					if err = sqlgraph.BatchCreate(ctx, acb.driver, spec); err != nil {
						if sqlgraph.IsConstraintError(err) {
							err = &ConstraintError{msg: err.Error(), wrap: err}
						}
					}
				}
				if err != nil {
					return nil, err
				}
				mutation.id = &nodes[i].ID
				if specs[i].ID.Value != nil && nodes[i].ID == 0 {
					id := specs[i].ID.Value.(int64)
					nodes[i].ID = int(id)
				}
				mutation.done = true
				return nodes[i], nil
			})
			for i := len(builder.hooks) - 1; i >= 0; i-- {
				mut = builder.hooks[i](mut)
			}
			mutators[i] = mut
		}(i, ctx)
	}
	if len(mutators) > 0 {
		if _, err := mutators[0].Mutate(ctx, acb.builders[0].mutation); err != nil {
			return nil, err
		}
	}
	return nodes, nil
}

// SaveX is like Save, but panics if an error occurs.
func (acb *AttendanceCreateBulk) SaveX(ctx context.Context) []*Attendance {
	v, err := acb.Save(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Exec executes the query.
func (acb *AttendanceCreateBulk) Exec(ctx context.Context) error {
	_, err := acb.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (acb *AttendanceCreateBulk) ExecX(ctx context.Context) {
	if err := acb.Exec(ctx); err != nil {
		panic(err)
	}
}
