package schema

import (
	"time"

	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

// User holds the schema definition for the User entity.
type User struct {
	ent.Schema
}

// Fields of the User.
func (User) Fields() []ent.Field {
	return []ent.Field{
		field.Int("id").Unique(),
		field.String("name"),
		field.String("email").Unique(),
		field.String("password"),
		field.String("role").Default("employee"),
		field.Bool("status").Default(true),
		field.Time("created_at").
			Default(time.Now),
		field.Time("update_at").Nillable().Optional(),
		field.Time("delete_at").Nillable().Optional(),
	}
}

// Edges of the User.
func (User) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("attendance", Attendance.Type),
	}

}
