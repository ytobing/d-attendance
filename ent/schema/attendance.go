package schema

import (
	"time"

	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

// Attendance holds the schema definition for the Attendance entity.
type Attendance struct {
	ent.Schema
}

// Fields of the Attendance.
func (Attendance) Fields() []ent.Field {
	return []ent.Field{
		field.Int("id").Unique(),
		field.String("clocked_in_date").Default(time.Now().Format("2006-01-02")),
		field.Time("clocked_in").Nillable().Optional(),
		field.Time("clocked_out").Nillable().Optional(),
		field.Time("created_at").
			Default(time.Now),
		field.Time("update_at").Nillable().Optional(),
		field.Time("delete_at").Nillable().Optional(),
	}
}

// Edges of the Attendance.
func (Attendance) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("user", User.Type).Ref("attendance").Unique(),
	}

}
