package main

import (
	"context"
	"fmt"
	"os"

	_ "entgo.io/ent"

	"github.com/joho/godotenv"
	_ "github.com/lib/pq"

	"gitlab.com/ytobing/d-attendance/common"

	"gitlab.com/ytobing/d-attendance/app"
)

func main() {
	godotenv.Load()
	ctx := context.Background()
	c := common.InitConfig(ctx)

	app.Init(c)

	c.Router.Logger.Fatal(c.Router.Start(fmt.Sprintf(":%s", os.Getenv("PORT"))))

}
